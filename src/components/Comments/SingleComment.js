import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import "../../App.css";

class SingleComment extends Component {

    state = {
        name: '',
        comment: '',
    };
    changeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };
    sendHandler = () => {
        this.props.sendComment(this.state.name, this.state.comment)
    };
render() {
return (
    <Form className="AddComment" inline>
        <FormGroup className="mb-2">
            <Label for="Name" >Name: </Label>
            <Input type="text" value={this.state.name} name="name" id="Name" onChange={this.changeHandler} placeholder="name" />
        </FormGroup>
        <FormGroup className="mb-2">
            <Label for="Comment" >Comment: </Label>
            <Input type="text" value={this.state.text} name="comment" id="Comment" onChange={this.changeHandler} placeholder="type a text here" />
        </FormGroup>
        <Button onClick={this.sendHandler}>ADD</Button>
    </Form>
);
}
}

export default SingleComment;