import axios from '../../axios';
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
export const COMMENT_SUCCESS = 'COMMENT_SUCCESS';

export const getNewsSuccess = (news) =>({type: NEWS_SUCCESS,news});
export const getCommentSuccess = (comments)=>({type: COMMENT_SUCCESS, comments});

export const getComments = ()=>{
    return dispatch =>{return axios.get('/comments').then(
        response =>{dispatch(getCommentSuccess(response.data))
        }
    )}
};
export const sendComment = ()=>{
    return(dispatch)=>{
        const comment ={
            name: '',
            text: '',
        };
        axios.post('/comments',comment).then(()=> dispatch(getComments()))
    }
};

export const getNews =()=>{
    return dispatch =>{return axios.get('/news').then(
        response => {dispatch(getNewsSuccess(response.data))}
    )}
};
export const postNews =()=>{
    return (dispatch) =>{
        const news = {
            title:'',
            text:'',
            date: '',
            img: null
        };
        axios.post('/',news).then(()=> dispatch(getNews()))
    }
};