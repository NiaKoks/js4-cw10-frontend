import {
    NEWS_SUCCESS,
    COMMENT_SUCCESS
} from "../actions/actions";

const initialState ={
    news:[],
    comments: [],
};

const Reducer =(state = initialState, action) =>{
    switch (action.type) {
        case NEWS_SUCCESS:
            return{
                ...state,
                news: action.news
            };
        case COMMENT_SUCCESS:
            return{
                ...state,
                comments: action.comments
            };
        default:
            return state;
    }
};

export default Reducer;