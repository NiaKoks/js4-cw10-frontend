import React, {Component} from 'react';
import {getComments} from "../../store/actions/actions";
import {connect} from "react-redux";

class Comments extends Component {
    componentDidMount() { this.props.getComments() }
    componentDidUpdate(){
        clearInterval(this.interval);
        const lastDatetime = this.props.getComments(this.props.comments.length && this.props.comments[this.props.comments.length -1].date);
        if(lastDatetime) this.interval = setInterval(this.props.getComments, 2000, lastDatetime)
    }
    render() {
        return (
            <div>{this.props.comments.map((message, id) => {
                return (
                    <div key={id}>
                        <span>{message.author} :</span>
                        <span>{message.message} </span>
                    </div>
                )})
            }
            </div>
        )
    }
}
const mapStateToProps= state =>({
    comments:state.reducer.comments
});

const mapDispatchToProps = dispatch =>({
    getComments: () => dispatch(getComments())
});

export default connect(mapStateToProps,mapDispatchToProps)(Comments);