import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {postNews} from "../../store/actions/actions";
import {connect} from "react-redux";
import "../../App.css"

class AddNews extends Component {

    state = {
        title: '',
        text: '',
        img: null
    };

    changeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    sendHandler = () => {
        this.props.postNews(this.state.title, this.state.text,this.state.date,this.state.img)
    };

    render() {
        return (
            <Form className="AddForm" inline>
                <FormGroup className="mb-2">
                    <Label for="Title" >Title: </Label>
                    <Input type="text" value={this.state.title} name="title" id="Title" onChange={this.changeHandler} placeholder="enter title" />
                </FormGroup>
                <FormGroup className="mb-2">
                    <Label for="Text" >Text: </Label>
                        <Input type="text" value={this.state.text} name="text" id="Text" onChange={this.changeHandler} placeholder="type a text here" />
                </FormGroup>
                <FormGroup>
                    <Input type="file" value={this.state.img} name="img" id="Image" onChange={this.changeHandler} placeholder="Choose file"/>
                </FormGroup>
                <Button onClick={this.sendHandler}>Send</Button>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        postNews: (title,text,img)  => dispatch(postNews(title,text,img)),
    }};


export default connect(null,mapDispatchToProps)(AddNews);