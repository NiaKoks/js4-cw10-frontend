import React,{Component} from "react";
import {Button, Col, Label,Card,CardTitle, CardImg, CardBody, CardHeader, CardText, Row} from "reactstrap";
import {Comments} from '../../components/Comments/Comments'
import {connect} from "react-redux";

import '../../App.css';
import {Link} from "react-router-dom";
import {getComments, getNews} from "../../store/actions/actions";
import SingleComment from "../Comments/SingleComment";

class SingleNews extends Component {
    state={
        news:{
            title:'',
            text:'',
            date: '',
            img: null
        },
        comments:[],
    };
    changeHandler = (event) =>{
        this.setState({[event.target.name]:event.target.value})
    };
    removeComment = id =>{
        const updatedListOfComments = [...this.state.comments];
        this.setState({updatedListOfComments});
    };

    componentDidMount() {
        this.props.getNews();
    }

    render() {
        console.log(this.props.news);
        return(
            <Row>
                <Col sm={12}>
                    <Link to="/news/add"><Button color="info" className="AddNews">Post new News</Button></Link>
                        <Card className="NewsCard">
                            <CardHeader>{this.news_title}</CardHeader>
                            <CardImg src={this.news_img} alt="no img"/>
                            <CardBody>
                                <CardTitle>{this.news_date}</CardTitle>
                                <CardText>{this.news_text}</CardText>
                            </CardBody>
                        </Card>
            <Label>Comments</Label>
                </Col>
            <Label>Add Comment</Label>
                <SingleComment/>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    news: state.news,
    comments: state.comments
});

const mapDispatchToProps = dispatch => ({
    getNews: () => dispatch(getNews()),
    getComments:() => dispatch(getComments())
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleNews)