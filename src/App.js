import React, { Component } from 'react';
import News from "./components/News/News";
import Comments from "./components/Comments/Comments"
import './App.css';
import AddNews from "./components/News/AddNews";
import {Route, Switch} from "react-router-dom";
import SingleNews from "./components/News/SingleNews";

class App extends Component {
  render() {
    return (
      <div className="App">
          <Switch>
              <Route path="/" exact component={News} />
              <Route path="/news/add" exact component={AddNews} />
              <Route path="/news/:id" exact component={SingleNews} />
          </Switch>
      </div>
    );
  }
}

export default App;
