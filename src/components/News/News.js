import React,{Component} from "react";
import {Button, Col, Form, FormGroup, Card, CardImg, CardBody, CardHeader, CardText, Row} from "reactstrap";
// import {postNews} from "../../store/actions/actions";
import {Comments} from '../../components/Comments/Comments'
import {connect} from "react-redux";

import '../../App.css';

import AddNews from "./AddNews";
import {Link} from "react-router-dom";
import {getNews} from "../../store/actions/actions";

class News extends Component {
    state={
      news:{
          title:'',
          text:'',
          date: '',
          img: null
      },
      comments:[],
    };
    changeHandler = (event) =>{
      this.setState({[event.target.name]:event.target.value})
    };
    removeNewsHandler = id =>{
        const updatedListOfNews = [...this.state.news];
        this.setState({updatedListOfNews});
    };
    componentDidMount() {
        this.props.getNews();
    }

    render() {
        console.log(this.props.news);
        return(
            <Row>
                <Col sm={12}>
                    <Link to="/news/add"><Button color="info" className="AddNews">Post new News</Button></Link>
                    {this.props.news.map(news => (
                        <Card className="NewsCard">
                            <CardHeader>{news.news_title}</CardHeader>
                            <CardImg src={news.news_img} alt="no img"/>
                            <CardBody>
                                <CardText>{news.news_text}</CardText>
                                <Link to={"/news/" + news.id}><Button color="link">Read More =></Button></Link>
                                <Button color="danger" className="DelNews">X</Button>
                            </CardBody>
                        </Card>
                    ))}

                </Col>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    news: state.news
});

const mapDispatchToProps = dispatch => ({
    getNews: () => dispatch(getNews())
});

export default connect(mapStateToProps, mapDispatchToProps)(News)